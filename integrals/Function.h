#ifndef FUNCTION_H
#define FUNCTION_H

class Function{

public:
	double parabole(double x);

private:
	double x;
};

#endif