#include "Integral.h"
#include "Function.h"

Integral::Integral(double a, double b, int n){
	this->a = a;
	this->b = b;
	this->n = n;
}

double Integral::integrate(){
	double h = (b - a) / n;
	double result = 0;

	for (int i = 0; i < n; i++){
		double x = i * h;

		Function function;
		result += function.parabole(x);
	}

	return result;
}