#ifndef INTEGRAL_H
#define INTEGRAL_H

class Integral{
public:
	Integral(double a, double b, int n);
	double integrate();
private:
	double a;
	double b;
	int n;
};

#endif