#include "integrals/Function.h"
#include "integrals/Integral.h"
#include <iostream>
int main(int argc, char ** argv){

	Integral integral(0, 10, 2);
	std::cout << integral.integrate() << std::endl;

	return 0;
}